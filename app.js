var express = require('express');
var bodyParser = require('body-parser');
var app = express();
var mongoose = require('mongoose');
var methodOverride = require('method-override');
var expressSanitizer = require('express-sanitizer');

// view engine setup
app.set('view engine', 'ejs');
app.use(bodyParser.urlencoded({ extended: true }));
app.use(expressSanitizer());
app.use(express.static("public"));
app.use(methodOverride("_method"));

mongoose.connect("mongodb://localhost/blog");
var blogSchema = new mongoose.Schema({
    title: String,
    image: String,
    body: String,
    created: {type: Date, default: Date.now}
});

var Blog = mongoose.model("Blog",blogSchema);


//Routes begin here

app.get("/",function(req,res){
    res.redirect("/blogs");
});

app.get("/blogs",function(req,res) {
    Blog.find({},function(err,bilog){
        if(err){
            console.log(err);
        }
        else{
            res.render("index",{blogs:bilog});
        }
    });

});

app.get("/blogs/new",function(req,res){
    res.render("new");
});

app.post("/blogs",function(req,res){
    //create and redirect
    req.body.blog.body = req.sanitize()
    Blog.create(req.body.blog,function(err,newBlog){
        if(err){
            console.log(err);
        }
        else{
            res.redirect("/blogs");

        }
    });

});

app.get("/blogs/:id",function(req,res){
    Blog.findById(req.params.id,function(err,showBlog){
        if(err){
            res.render("/blogs");
        }
        else{
            res.render("show",{blog:showBlog});
        }
    });
});

app.get("/blogs/:id/edit",function(req,res){
    req.body.blog.body = req.sanitize()
    Blog.findById(req.params.id,function(err,fb){
        if(err){
            res.redirect("/blogs");
        }
        else{
            res.render("edit",{blog:fb});
        }
    });
});

app.put("/blogs/:id",function(req,res){
    Blog.findByIdAndUpdate(req.params.id,req.body.blog,function(err,updated){
        if(err){
            res.redirect("/blogs");
        }
        else{
            res.redirect("/blogs/"+req.params.id);
        }
    });

});

app.delete("/blogs/:id",function(req,res){
    Blog.findByIdAndRemove(req.params.id,function(err){
        if(err){
            res.redirect("/blogs");
        }
        else{
            res.redirect("/blogs");
        }
    });
});





app.listen(3000);
