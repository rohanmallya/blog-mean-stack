# Blog Model 

## What is it?

A blog model created using Nodejs, ExpressJS and MongoDB. Semantic-UI was used to stylize the webpages. Currently supports Create, Read, Update and Destroy

## Known Issues


* Input Checking is not done in form submission
* Styling pending in some routes

## Routes

`/blogs` - Fetches all blogs
`/blogs/new` - To create a new blog entry
`/blogs/<id>` - Gets the blog post with `id`
`/blogs/:id/edit` - Edits blog post with id `id`
 
